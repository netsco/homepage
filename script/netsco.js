var csschage = false;
var devices = {
    'width' : [1280,1024,768,480,320],
    'name' : ["m-desktop","m-tablet-land","m-tablet-port","m-mobile-land","m-mobile-port"]
};

var num_devices = devices['width'].length;

function init() {
    if ( $(window).width() < 1280){
        $("header").hide();
        $(".empty-top").hide();
        jump($(window).width());
    }

    $(window).bind("scroll", function() {
        var nowtop = $(document).scrollTop();
        if (nowtop > $("#container .menus").position().top + 90) {
            $("#container .menus-float").show();
        } else {
            $("#container .menus-float").hide();
        }

    });
    $("#container .middle .item").bind("click", function() {
        var offset = 50;
        if ( $("#container").width() <= 320 ){
            offset = 40;
        }else if ( $("#container").width() <= 480 ){
            offset = 50;
        }
        var target = $(this).attr('anchor');
        if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i))) {
            var to = $("#" + target).position().top - offset;
            $("html:not(:animated),body:not(:animated)").stop().animate({ scrollTop: to }, 100, "swing", function(evt) {
                $(".menus-float").css({ "position": "relative" });
                window.scroll(0, to );
                $(".menus-float").css({ "position": "fixed" });
            });
        }else {
            $(document).scrollTop($("#" + target).position().top - offset);
        }


        $("#container .middle .item .icons").removeClass('selected');
        $("#container .middle .item .icons .click").hide();
        $("#container .middle .item .icons .unclick").show();

        $("#container .middle .item[anchor=" + target +"] .icons").addClass('selected');
        $("#container .middle .item[anchor=" + target +"] .icons").find(".click").show();
        $("#container .middle .item[anchor=" + target +"] .icons").find(".unclick").hide();

    });

    $("header .platform li").bind('click', function() {
        var index = jQuery.inArray($(this).attr('id'), devices['name']);
        jump(devices['width'][index]);
    });

    if ( $(window).width() >= 1280){
        $("header .platform li:first").click();
    }
}

$(window).resize(function() {
    if (!csschage) {
        jump($(window).width());
    }
});

function topiconselector(name){
    $("header .platform li").removeClass('selected');
    $("header .platform li .click").hide();
    $("header .platform li .unclick").show();
    $("#" + name).addClass('selected');
    $("#" + name).find(".click").show();
    $("#" + name).find(".unclick").hide();
}

function jump(width) {
    csschage = true;
    var cssflex = $("#flexible");
    var name;

    if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i)) ) {
        if ( width < 480 ){
            name = "m-mobile-port";

        }
        else if ( width < 768 ){
            name = "m-mobile-land";

        }
        else if ( width < 1024 ){
            name = "m-tablet-port";

        }
        else if ( width < 1280 ){
            name = "m-tablet-land";

        }
        else {
            name = "m-desktop";

        }
    }else {
        if ( width < 480 ){
            name = "m-mobile-port";
            cssflex.attr("href","../css/over320.css");
        }
        else if ( width < 768 ){
            name = "m-mobile-land";
            cssflex.attr("href","../css/over480.css");
        }
        else if ( width < 1024 ){
            name = "m-tablet-port";
            cssflex.attr("href","../css/over768.css");
        }
        else if ( width < 1280 ){
            name = "m-tablet-land";
            cssflex.attr("href","../css/over1024.css");
        }
        else {
            name = "m-desktop";
            cssflex.attr("href","../css/over1280.css");
        }

    }


    var index = jQuery.inArray(name, devices['name']);

    $("#container").stop().animate({
        width: devices['width'][index]
    }, 200, function() { csschage = false; });
    topiconselector(name);

}